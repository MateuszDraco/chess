#!/bin/bash

#docker run -d -p 49153:6379 -i -t --name chess_redis ubu_with_redis /usr/bin/redis-server

function stop() {
  docker stop chess_client
  docker stop chess_worker
}

case "$1" in

build)
	echo "building..."
	docker build -t mateusz/scala:latest scala
	docker build -t mateusz/chess chess
	;;

init)
	echo "initializing..."
	docker run -d --expose 12553 --name chess_worker -v /chess mateusz/chess worker
	docker run -d -p 49154:80 --expose 12554 --name chess_client -v /chess --link chess_worker:worker mateusz/chess client
	echo "connect with localhost:49154"
	;;

start)
	docker start chess_worker
	docker start chess_client
	;;

stop)
  stop
	;;

remove)
  stop
  docker rm chess_worker
  docker rm chess_client
  ;;

demolish)
  docker rmi mateusz/chess
  ;;


*)
	echo "start with: build, init, start or stop."
  ;;

esac
