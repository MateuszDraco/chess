#!/bin/bash

function update() {
	echo "updating...."
	cd /chess

	git pull

	echo "compiling common...."
	cd common
	sbt clean
	sbt publish-local
}

case "$1" in

worker)
	echo "starting WORKER...."
	update
	cd /chess/worker
	sbt clean compile
	echo "see you at my port..."
	sbt run
	;;

client)
	echo "starting CLIENT...."
	update
	cd /chess/client
	play clean stage
	echo "see you at 80 port..."
	target/universal/stage/bin/client -Dhttp.port=80 -Dapplication.secret="twCjEhgC9wtm" -Dchess.worker.ip=$WORKER_PORT_12553_TCP_ADDR -Dchess.worker.port=$WORKER_PORT_12553_TCP_PORT
	;;

console)
	echo "starting console...."
	/bin/bash
	;;

*)
	echo "You fu**ed up. It won't work this way."
	;;

esac
