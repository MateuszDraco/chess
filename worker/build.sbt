name := "ChessWorker"

version := "1.0"

scalaVersion := "2.10.3"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-remote" % "2.2.4",
  "org.scalatest" % "scalatest_2.10" % "2.1.3" % "test",
  "com.mdraco" %% "chess-common" % "1.1-SNAPSHOT"
)

scalacOptions += "-feature"
