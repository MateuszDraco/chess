package chess

import com.typesafe.config.ConfigFactory
import akka.actor.ActorSystem
import akka.actor.Props

/**
 *
 * User: mateusz
 * Date: 25.04.2014
 * Time: 18:58
 * Created with IntelliJ IDEA.
 */
object ChessCalculator {
  def main(args: Array[String]): Unit = {
    val system = ActorSystem("ChessSystem", ConfigFactory.load("worker"))
    system.actorOf(Props[GameActor], "ChessMechanics")

    println("Started CalculatorSystem - waiting for messages")
  }
}
