package chess

import chess.pieces._
import chess.common.data.{Position, GameState, PieceState}

/**
 *
 * User: mateusz
 * Date: 26.04.2014
 * Time: 00:14
 * Created with IntelliJ IDEA.
 */
object Game {
  def move(state: GameState, piece: ChessPiece, from: Position, to: Position): GameState = {
    val whites = if (state.currentWhite) piece(to) :: state.whites.filter(_.position != from) else state.whites.filter(_.position != to)
    val blacks = if (state.currentWhite) state.blacks.filter(_.position != to) else piece(to) :: state.blacks.filter(_.position != from)
    GameState(!state.currentWhite, whites, blacks)
  }

  def create = GameState(currentWhite = true, defaultSet(0, 1), defaultSet(7, 6))

  private def defaultSet(row1: Int, row2: Int): List[PieceState] = {
    val direction = if (row2 > row1) 1 else -1
    val first = if (direction > 0) 0 else 7

    val pawn = Pawn(row2) _
    val rook = Rook(row1) _
    val knight = Knight(row1) _
    val bishop = Bishop(row1) _

    List(
      rook(first),
      pawn(first),
      knight(first + direction),
      pawn(first + direction),
      bishop(first + direction * 2),
      pawn(first + direction * 2),
      King(row1)(first + direction * 3),
      pawn(first + direction * 3),
      Queen(row1)(first + direction * 4),
      pawn(first + direction * 4),
      bishop(first + direction * 5),
      pawn(first + direction * 5),
      knight(first + direction * 6),
      pawn(first + direction * 6),
      rook(first + direction * 7),
      pawn(first + direction * 7)
    )
  }

}
