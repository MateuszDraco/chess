package chess

import akka.actor.Actor
import chess.common.events._
import chess.common.data.GameState
import scala.util.Random
import chess.common.events.MakeAMove
import chess.common.events.InvalidMove
import scala.Some
import chess.common.events.GameStarted
import chess.pieces.{King, ChessPiece}
import scala.collection.mutable

/**
 *
 * User: mateusz
 * Date: 25.04.2014
 * Time: 19:09
 * Created with IntelliJ IDEA.
 */
class GameActor extends Actor {
  var games: mutable.Map[String, GameState] = mutable.Map()

  override def receive = {
    case StartGame =>
      println("starting new game")

      val id = Random.alphanumeric.take(20).mkString
      val state = Game.create
      games = games + (id -> state)
      sender ! GameStarted(id, state)

    case MakeAMove(gameId: String, from, to) =>
      val response: MoveResult = games.get(gameId) match {
        case Some(game) =>
          val pieces = if (game.currentWhite) game.whites else game.blacks
          pieces.find(_.position == from) match {
            case None =>
              InvalidMove("there is no valid piece in start position")
            case Some(piece) =>
              val kind = ChessPiece(piece.kind)
              if (kind.possibleMoves(from, game).exists(_ == to)) {
                val state = Game.move(game, kind, from, to)
                if (state.currentWhite && state.whites.exists(_.kind == King.name) || !state.currentWhite && state.blacks.exists(_.kind == King.name)) {
                  games(gameId) = state
                  ValidMove(state)
                }
                else {
                  self ! CloseAGame(gameId)
                  KingWasKilled
                }
              } else {
                InvalidMove("that piece cannot go there")
              }
          }
        case None =>
          InvalidMove("there is no such a game")
      }

      sender ! response
  }
}
