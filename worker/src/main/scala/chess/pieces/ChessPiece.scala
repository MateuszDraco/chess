package chess.pieces

import chess.common.data.{PieceState, Position, GameState}

/**
 *
 * User: mateusz
 * Date: 25.04.2014
 * Time: 19:53
 * Created with IntelliJ IDEA.
 */
trait ChessPiece {
  val name: String

  def possibleMoves(from: Position, game: GameState): List[Position]

  def apply(position: Position): PieceState = PieceState(name, position)

  //def apply(row: Int, col: Int) = apply(Position(row, col))

  def apply(row: Int)(col: Int): PieceState = apply(Position(row, col))
}

object ChessPiece {
  def apply(name: String): ChessPiece = name match {
    case King.name => King
    case Queen.name => Queen
    case Bishop.name => Bishop
    case Knight.name => Knight
    case Rook.name => Rook
    case Pawn.name => Pawn
  }
}