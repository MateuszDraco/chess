package chess.pieces

import chess.common.data.{GameState, Position}

/**
 *
 * User: mateusz
 * Date: 25.04.2014
 * Time: 23:03
 * Created with IntelliJ IDEA.
 */
object Knight extends ChessPiece {
  override def possibleMoves(from: Position, game: GameState): List[Position] = {
    val isWhite = game.white(from)
    val possibleMoves: List[Option[Position]] = from.move("ull") ::
      from.move("uul") ::
      from.move("uur") ::
      from.move("urr") ::
      from.move("drr") ::
      from.move("ddr") ::
      from.move("ddl") ::
      from.move("dll") ::
      from.move("ull") ::
      List()

    possibleMoves.foldLeft(List[Position]()) { (list, pos) =>
      pos.fold(list)(p =>
        if (isWhite && game.white(p) || !isWhite && game.black(p))
          list
        else
          p :: list)
    }
  }

  override val name: String = "Knight"
}
