package chess.pieces

import chess.common.data.{GameState, Position}

/**
 *
 * User: mateusz
 * Date: 25.04.2014
 * Time: 20:55
 * Created with IntelliJ IDEA.
 */
object Pawn extends ChessPiece {
  override def possibleMoves(from: Position, game: GameState): List[Position] = {
    val isWhite = game.white(from)
    def tryForward: List[Position] = {
      val next = if (isWhite) from.down else from.up
      next match {
        case None => List()
        case Some(pos) =>
          if (game.any(pos))
            List()
          else if (isWhite && pos.row == 1 && !game.any(Position(3, pos.col)))
            List(pos, Position(3, pos.col))
          else if (!isWhite && pos.row == 6 && !game.any(Position(4, pos.col)))
            List(pos, Position(4, pos.col))
          else
            List(pos)
      }
    }
    def tryAttack: List[Position] = {
      val possibilities = if (isWhite) List(from.move("dl"), from.move("dr")) else List(from.move("ul"), from.move("ur"))
      val enemies = if (isWhite) game.blacks else game.whites
      possibilities.foldLeft(List[Position]())((agg, opos) => opos match {
        case None => agg
        case Some(pos) => if (enemies.exists(_.position == pos)) pos :: agg else agg
      })
    }

    tryForward ++ tryAttack
  }

  override val name: String = "P"
}
