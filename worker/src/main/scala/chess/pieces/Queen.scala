package chess.pieces

import chess.common.data.{GameState, Position}

/**
 *
 * User: mateusz
 * Date: 25.04.2014
 * Time: 23:02
 * Created with IntelliJ IDEA.
 */
object Queen extends ChessPiece {
  override def possibleMoves(from: Position, game: GameState): List[Position] = {
    val isWhite = game.white(from)
    def move(start: Position, direction: String, previous: List[Position]): List[Position] = start.move(direction) match {
      case Some(destination) =>
        if ((isWhite && game.white(destination)) || (!isWhite && game.black(destination)))
          previous
        else if ((isWhite && game.black(destination)) || (!isWhite && game.white(destination)))
          destination :: previous
        else
          move(destination, direction, destination :: previous)
      case None =>
        previous
    }
    move(from, "u", List()) ++
      move(from, "d", List()) ++
      move(from, "l", List()) ++
      move(from, "r", List()) ++
      move(from, "ur", List()) ++
      move(from, "dr", List()) ++
      move(from, "dl", List()) ++
      move(from, "ul", List())
  }

  override val name: String = "Queen"
}
