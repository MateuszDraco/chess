package chess.pieces

import chess.common.data.{Position, GameState}

/**
 *
 * User: mateusz
 * Date: 25.04.2014
 * Time: 20:17
 * Created with IntelliJ IDEA.
 */
object King extends ChessPiece {
  override val name: String = "King"

  override def possibleMoves(from: Position, game: GameState): List[Position] = {
    val white = game.white(from)
    val friends = if (white) game.whites else game.blacks
    List(from.up, from.right, from.down, from.left, from.move("ur"), from.move("rd"), from.move("dl"), from.move("lu")).foldLeft(List[Position]())((agg, possible) => possible match {
      case None => agg
      case Some(position) => if (friends.exists(_.position == position)) agg else position :: agg
    })
  }
}
