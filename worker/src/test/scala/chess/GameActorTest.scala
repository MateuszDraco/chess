package chess

import org.scalatest.FlatSpec
import akka.actor.{ActorSystem, Props}
import chess.common.events._
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps
import chess.common.data.Position
import chess.common.events.GameStarted
import chess.common.events.MakeAMove

/**
 *
 * User: mateusz
 * Date: 26.04.2014
 * Time: 00:41
 * Created with IntelliJ IDEA.
 */
class GameActorTest extends FlatSpec {
  "A game" should "start with proper pieces" in {
    val system = ActorSystem("ChessSystem")
    val actor = system.actorOf(Props[GameActor], "ChessCalculator")
    implicit val timeout = Timeout(1 second)

    val createResponse = (actor ? StartGame).mapTo[GameStarted]
    val state = Await.result(createResponse, 1 second).state

    assert(state.whites.size == 16)
    assert(state.blacks.size == 16)
  }

  it should "prevent incorrect piece color move" in {
    val system = ActorSystem("ChessSystem")
    val actor = system.actorOf(Props[GameActor], "ChessCalculator")
    implicit val timeout = Timeout(1 second)

    val createResponse = (actor ? StartGame).mapTo[GameStarted]
    val id = Await.result(createResponse, 1 second).id

    val move = (actor ? MakeAMove(id, Position(6, 2), Position(5, 2))).mapTo[MoveResult]
    move map {
      case InvalidMove(_) => assert(true)
      case _ => assert(false)
    }
  }


  it should "change sides after move" in {
    val system = ActorSystem("ChessSystem")
    val actor = system.actorOf(Props[GameActor], "ChessCalculator")
    implicit val timeout = Timeout(1 second)

    val createResponse = (actor ? StartGame).mapTo[GameStarted]
    val id = Await.result(createResponse, 1 second).id

    val move = (actor ? MakeAMove(id, Position(1, 2), Position(3, 2))).mapTo[MoveResult]
    move map {
      case ValidMove(state) => assert(!state.currentWhite)
      case _ => assert(false)
    }
  }
}
