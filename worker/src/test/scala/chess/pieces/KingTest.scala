package chess.pieces

import org.scalatest._
import chess.common.data.{Position, GameState}
import chess.Game

/**
 *
 * User: mateusz
 * Date: 25.04.2014
 * Time: 21:43
 * Created with IntelliJ IDEA.
 */
class KingTest extends FlatSpec {
  "A king" should "be able to move in any direction" in {
    val position: Position = Position(4, 4)
    val emptyState = GameState(currentWhite = true, List(King(position)), List())

    val possibilities = King.possibleMoves(position, emptyState)
    val expected = List(position.up.get,
      position.move("ur").get,
      position.right.get,
      position.move("rd").get,
      position.down.get,
      position.move("dl").get,
      position.left.get,
      position.move("lu").get
    ).toSet
    assert(expected == possibilities.toSet)
    assert(8 == possibilities.size)
    assert(8 == possibilities.toSet.size)
    assert(8 == expected.toSet.size)
  }

  it should "attack foes and respect friends" in {
    val position = Position(4, 4)
    val state = GameState(currentWhite = true,
      List(
        King(position),
        Pawn(Position(3, 5)),
        Pawn(Position(4, 3)),
        Pawn(Position(7, 7))
      ),
      List(
        Pawn(Position(5, 3)),
        Pawn(Position(4, 5)),
        Pawn(Position(1, 1)),
        Pawn(Position(6, 2)),
        Pawn(Position(4, 6))
      )
    )

    val expected = Set(
      Position(3, 4),
      //Position(3, 5)
      Position(4, 5),
      Position(5, 5),
      Position(5, 4),
      Position(5, 3),
      //Position(4, 3)
      Position(3, 3)
    )

    assert(King.possibleMoves(position, state).toSet == expected)
  }

  it should "dont have any moves at start" in {
    val state = Game.create
    val position = state.whites.filter(_.kind == King.name)
    assert(position.size == 1)
    assert(King.possibleMoves(position(0).position, state).isEmpty)
  }

}
