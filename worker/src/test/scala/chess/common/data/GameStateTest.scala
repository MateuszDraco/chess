package chess.common.data

import org.scalatest.FlatSpec
import chess.pieces.{King, Pawn}

/**
 *
 * User: mateusz
 * Date: 25.04.2014
 * Time: 22:56
 * Created with IntelliJ IDEA.
 */
class GameStateTest extends FlatSpec {
  "A piece" should "belongs to its kind" in {
    val position = Position(4, 4)
    val state = GameState(true,
      List(
        King(position),
        Pawn(Position(3, 5)),
        Pawn(Position(4, 3)),
        Pawn(Position(7, 7))
      ),
      List(
        Pawn(Position(5, 3)),
        Pawn(Position(4, 5)),
        Pawn(Position(1, 1)),
        Pawn(Position(6, 2)),
        Pawn(Position(4, 6))
      )
    )

    assert(state.white(position))
    assert(!state.black(position))
  }
}
