package chess.common.data

import org.scalatest.{FlatSpec}

/**
 *
 * User: mateusz
 * Date: 25.04.2014
 * Time: 22:25
 * Created with IntelliJ IDEA.
 */
class PositionTest extends FlatSpec {
  "A position" should "have 8 position around" in {
    val position = Position(4, 4)
    val around = List(
      position.up.get,
      position.move("ur").get,
      position.right.get,
      position.move("rd").get,
      position.down.get,
      position.move("dl").get,
      position.left.get,
      position.move("lu").get
    )
    val ok = List(
      Position(3, 4),
      Position(3, 5),
      Position(4, 5),
      Position(5, 5),
      Position(5, 4),
      Position(5, 3),
      Position(4, 3),
      Position(3, 3)
    )
    assert(ok == around)
  }

  it should "move left properly" in {
    val position = Position(4, 4)
    val l1 = Position(4, 3)
    val l2 = Position(4, 2)

    assert(l1 == position.left.get)
    assert(l1 == position.move("l").get)

    assert(l2 == position.left.get.left.get)
    assert(l2 == position.move("ll").get)
  }
}
