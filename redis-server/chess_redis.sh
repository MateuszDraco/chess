#!/bin/bash

#docker run -d -p 49153:6379 -i -t --name chess_redis ubu_with_redis /usr/bin/redis-server

case "$1" in

build)
	echo "building..."
	docker build -t mateusz/redis:latest - < redis.Dockerfile
	;;

init)
	echo "initializing..."
	#docker run -d -p 49153:6379 -i -t --name chess_redis ubu_with_redis /usr/bin/redis-server
	docker run -d -p 49153:6379 -i -t --name chess_redis mateusz/redis:latest
	echo "connect with localhost:49153"
	;;

start)
	docker start chess_redis
	;;

stop)
	docker stop chess_redis
	;;

*)
	echo "start with: build, init, start or stop."

esac

