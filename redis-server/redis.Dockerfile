# Redis server
#
# version 1.0

FROM ubuntu:14.04

MAINTAINER M. Bednarski, m@mdraco.com

RUN apt-get update

RUN apt-get -y upgrade

RUN apt-get install -y gcc make g++ build-essential libc6-dev tcl wget

RUN (wget http://download.redis.io/redis-stable.tar.gz -O - | tar -xvz)

RUN (cd /redis-stable && make)

RUN mkdir -p /usr/local/bin

RUN cp /redis-stable/src/redis-server /usr/local/bin/
RUN cp /redis-stable/src/redis-cli /usr/local/bin/

EXPOSE 6379

ENTRYPOINT ["/usr/local/bin/redis-server"]
