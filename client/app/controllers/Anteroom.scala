package controllers

import play.api.mvc.{WebSocket, Action, Controller}
import play.api.libs.iteratee.Concurrent.Channel
import play.api.libs.json.{JsArray, JsString, JsObject, JsValue}
import scala.util.Random
import play.api.libs.iteratee.{Iteratee, Enumerator, Concurrent}
import play.api.Logger
import scala.concurrent.ExecutionContext.Implicits.global

/**
 *
 * User: mateusz
 * Date: 01.05.2014
 * Time: 16:35
 * Created with IntelliJ IDEA.
 */
object NameCreator {
  val firsts = List("Hungry", "Naughty", "Spiced", "Clever", "Fat", "Slimy", "Oozy", "Monstrous", "Filthy", "Smart", "Short", "Dark", "Dummy", "Vicious", "Clever")
  val seconds = List("Midget", "Badger", "Alien", "Horse", "Jose", "Chick", "Chicken", "Rhino", "Void", "Ghost", "Redneck", "Snail", "Fairy", "John", "Johnny", "Peter", "Snowman", "BatMan")

  def create = firsts(Random.nextInt(firsts.length)) + seconds(Random.nextInt(seconds.length))
}

case class Gamer(id: String, name: String, channel: Channel[JsValue]) {
  def rename(newName: String) = Gamer(id, newName, channel)
  def introduce = JsObject(Seq(
    "id" -> JsString(id),
    "name" -> JsString(name)
  ))
}

trait GamersManager {
  private var gamers: scala.collection.mutable.Map[String, Gamer] = scala.collection.mutable.Map[String, Gamer]()

  protected def newGamerId = Random.alphanumeric.take(20).mkString

  private def sendWelcomeTo(gamer: Gamer) = gamer.channel.push(JsObject(Seq(
    "what" -> JsString("welcome"),
    "me" -> gamer.introduce
  )))

  private def sendGamersTo(gamer: Gamer) = gamer.channel.push(JsObject(Seq(
    "what" -> JsString("gamers"),
    "gamers" -> JsArray(
      all.map(_.introduce).toSeq
    )))
  )

  private def sendNotificationAbout(gamer: Gamer) = broadcast(
    "what" -> JsString("greenhorn"),
    "who" -> gamer.introduce
  )

  private def add(gamer: Gamer) = {
    sendWelcomeTo(gamer)
    sendGamersTo(gamer)
    sendNotificationAbout(gamer)
    gamers += gamer.id -> gamer
  }

  def addGamer(id: String): Enumerator[JsValue] = {
    val name = NameCreator.create

    Concurrent.unicast[JsValue](
      ch => add(Gamer(id, name, ch)),
      () => {
        Logger.debug(s"--. closing for $id and trying to remove...")
        removeGamer(id, notifyOthers = true)
      }
    )
  }

  def sendQuitMessage(id: String) = broadcast(
    "what" -> JsString("quited"),
    "who" -> JsString(id)
  )

  def removeGamer(id: String, notifyOthers: Boolean) = gamers.get(id).map { gamer =>
    Logger.debug(s"-- removing $id")
    gamer.channel.eofAndEnd()
    gamers -= id
    if (notifyOthers)
      sendQuitMessage(id)
  }.get

  def renameGamer(id: String, newName: String) = gamers.get(id).map { gamer =>
    gamers(id) = gamer.rename(newName)
    broadcast(
      "what" -> JsString("rename"),
      "who" -> JsString("id"),
      "name" -> JsString(newName)
    )
  }.get

  private def all = gamers.values

  def getGamer(id: String) = gamers.get(id)

//  private def broadcast(data: JsObject) = all.map { g =>
//    g.channel.push(data)
//  }

  def broadcast(data: (String, JsValue)*) = {
    val json = JsObject(data)
    all.map {
      g =>
        g.channel.push(json)
    }
  }

}

object Anteroom extends Controller with GamersManager {

  def join = Action { implicit request =>
    val id = newGamerId
    Logger.debug(s"new player entering the anteroom: $id")
    Ok(views.html.anteroom()).withSession(request.session + ("gamerId", id))
  }

  def open = WebSocket.using[JsValue] { implicit request =>
    val id = request.session.get("gamerId").getOrElse(newGamerId)
    (onMessage(id), addGamer(id))
  }

  private def onMessage(gamerId: String): Iteratee[JsValue, Unit] = Iteratee.foreach[JsValue] { message =>
    (message \ "what").asOpt[String] match {
      case Some("rename") =>
        (message \ "name").asOpt[String].map { name =>
          renameGamer(gamerId, name)
        }.get

      case Some("start") =>
        (message \ "opponent").asOpt[String].map { opponentId =>
          getGamer(opponentId).map { opponent =>
            getGamer(gamerId).map { me =>
              Chess.prepare(me.id, opponent.id).map {
                case matchId: String =>

                  val sign = JsObject(Seq(
                    "what" -> JsString("start"),
                    "match" -> JsString(matchId)
                  ))

                  me.channel.push(sign)
                  opponent.channel.push(sign)

                  removeGamer(gamerId, notifyOthers = false)
                  removeGamer(opponentId, notifyOthers = false)

                  broadcast(
                    "what" -> JsString("matchStarted"),
                    "gamer1" -> JsString(gamerId),
                    "gamer2" -> JsString(opponentId)
                  )
              }

            }.get
          }.get
        }.get

      case Some("quit") =>
        removeGamer(gamerId, notifyOthers = true)

      case Some("say") =>
        val text = (message \ "text").asOpt[String].getOrElse("")
        broadcast(
          "what" -> JsString("say"),
          "who" -> JsString(gamerId),
          "said" -> JsString(text)
        )

      case _ =>
        Logger.debug("something goes wrong in anteroom")
    }
  }.map {
    _ =>
      //Logger.debug("-- quit: iteratee finished, trying to remove...")
      removeGamer(gamerId, notifyOthers = true)
  }
}
