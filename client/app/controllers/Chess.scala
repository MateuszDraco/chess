package controllers

import play.api.mvc.{WebSocket, Action, Controller}
import akka.actor._
import play.api.libs.json._
import play.api.libs.iteratee.Concurrent.Channel
import play.api.libs.iteratee._
import scala.collection.mutable
import play.api.Logger
import akka.pattern.ask
import concurrent.duration._
import akka.util.Timeout
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import com.typesafe.config.ConfigFactory
import chess.common.events._
import chess.common.data._
import play.api.libs.json.JsArray
import chess.common.events.GameStarted
import chess.common.events.InvalidMove
import chess.common.data.GameState
import play.api.libs.json.JsString
import play.api.libs.json.JsBoolean
import scala.Some
import play.api.libs.json.JsNumber
import chess.common.events.ValidMove
import chess.common.data.PieceState
import chess.common.events.MakeAMove
import chess.common.events.CloseAGame
import akka.actor.ActorIdentity
import akka.actor.Identify
import akka.actor.Terminated
import play.api.libs.json.JsObject

/**
 *
 * User: mateusz
 * Date: 26.04.2014
 * Time: 11:17
 * Created with IntelliJ IDEA.
 */

class RemoteGameActor(path: String) extends Actor {
  override def receive: Receive = identifying
  private implicit val askTimeout = Timeout(5.seconds)

  //sendIdentityRequest()

  def workingWith(remoteActor: ActorRef): Actor.Receive = {
    case StartGame =>
      Logger.debug("sending start message")
      val cryBaby = sender
      //sender ! "fruuu"
      (remoteActor ? StartGame).mapTo[ChessResponse].map { response =>
        //Logger.debug("match have started remotely")
        cryBaby ! response
      }
    case move: MakeAMove =>
      val cryBaby = sender
      (remoteActor ? move).map { r =>
        cryBaby ! r
      }
    case close: CloseAGame =>
      remoteActor ! close
    case Terminated(`remoteActor`) =>
      Logger.info("remote actor was closed")
      context.become(identifying)
      sendIdentityRequest()
    case ReceiveTimeout =>
      //Logger.debug("hooray, we have met the expectations!")
  }

  def sendIdentityRequest(): Unit = {
    context.actorSelection(path) ! Identify(path)
    import akka.actor.ReceiveTimeout
    import scala.concurrent.duration._
    context.system.scheduler.scheduleOnce(3.seconds, self, ReceiveTimeout)
  }

  def identifying: Actor.Receive = {
    case ActorIdentity(`path`, Some(actor)) =>
      Logger.debug("remote actor connected")
      context.watch(actor)
      context.become(workingWith(actor))
    case ActorIdentity(`path`, None) =>
      Logger.info(s"Remote actor not available: $path")
    case ReceiveTimeout =>
      sendIdentityRequest()
//    case request: ChessRequest =>
//      import play.api.libs.concurrent.Execution.Implicits._
//      Akka.system.scheduler.scheduleOnce(500.milliseconds, self, request)
    case _ =>
      Logger.debug(s"actor is not yet ready: ")
  }

}

object Chess extends Controller {
  //private def newGameId = Random.alphanumeric.take(20).mkString

  private implicit val askTimeout = Timeout(5.seconds)

  private val remotePath: String = {
    import play.api.Play.current

    val config = play.api.Play.application.configuration
    val ip = config.getString("chess.worker.ip").getOrElse("127.0.0.1")
    val port = config.getString("chess.worker.port").getOrElse("12553")
    Logger.debug(s"connecting to worker at $ip:$port")
    s"akka.tcp://ChessSystem@$ip:$port/user/ChessMechanics"
  }
  private val system = ActorSystem("RemoteChess", ConfigFactory.load("client"))
  private val remoteGames = system.actorOf(Props(classOf[RemoteGameActor], remotePath))

  remoteGames ! ReceiveTimeout

  private case class GameData(
      white: String,
      black: String,
      gamers: Map[String, String],
      channel: Channel[JsValue],
      enumerator: Enumerator[JsValue],
      state: GameState) {

    def gamersJson: JsObject = {
      val gamersJson = gamers.map {
        kv =>
          JsObject(Seq(
            "id" -> JsString(kv._1),
            "name" -> JsString(kv._2)
          ))
      }.toSeq

      Logger.debug(s"sending players [${gamers.size}}]...")
      JsObject(Seq(
        "what" -> JsString("players"),
        "players" -> JsArray(gamersJson)
      ))
    }

    def stateJson: JsObject = {
      def piecesToJson(states: List[PieceState]): JsArray = JsArray(states.map {
        piece =>
          //Logger.debug(s"   ${piece.kind} at row=${piece.position.row} col=${piece.position.col}")
          JsObject(Seq(
            "kind" -> JsString(piece.kind),
            "row" -> JsNumber(piece.position.row),
            "col" -> JsNumber(piece.position.col)
          ))
      }.toSeq)

      //Logger.debug("whites:")
      val whiteJson = piecesToJson(state.whites)
      //Logger.debug("blacks:")
      val blackJson = piecesToJson(state.blacks)

      val stateJson = JsObject(Seq(
        "isWhite" -> JsBoolean(state.currentWhite),
        "blacks" -> blackJson,
        "whites" -> whiteJson
      ))

      JsObject(Seq(
        "what" -> JsString("state"),
        "state" -> stateJson
      ))
    }


    def join(id: String, name: String): GameData =
      GameData(white, black, gamers + (id -> name), channel, enumerator, state)

    def quits(id: String): GameData =
      GameData(white, black, gamers - id, channel, enumerator, state)

    def change(newState: GameState): GameData =
      GameData(white, black, gamers, channel, enumerator, newState)

  }

  private var games: mutable.Map[String, GameData] = mutable.Map()

  def prepare(gamer1: String, gamer2: String): Future[String] = {
    (remoteGames ? StartGame).mapTo[GameStarted].map { game =>
      Logger.debug(s"match prepared for $gamer1 and $gamer2")
      val (out, channel) = Concurrent.broadcast[JsValue]
      games += game.id -> GameData(gamer1, gamer2, Map(), channel, out, game.state)

      game.id
    }
  }

  def game(gameId: String, myName: String) = Action { implicit request =>
    request.session.get("gamerId").fold {
      Redirect(routes.Application.index()).withNewSession
    } {
      playerId =>
        Logger.debug(s"open game $gameId for $playerId ($myName)")
        val title: String = games.get(gameId).fold("???")(game =>
          if (game.white == playerId)
            "wigga"
          else if (game.black == playerId)
            "nigga"
          else
            "stalker"
        )
        Ok(views.html.game(gameId, playerId, myName, title)).withSession(session + ("gamerName" -> myName))
    }
  }

  def open(gameId: String) = WebSocket.using[JsValue] { implicit request =>
    val connect = for {
      g <- games.get(gameId)
      p <- request.session.get("gamerId")
      n <- request.session.get("gamerName")
    } yield {
      Logger.debug(s"new player ($p, $n) on $gameId")
      val state = g.join(p, n)
      games(gameId) = state

      (player(gameId, p, state.channel), state.enumerator)
    }

    connect.getOrElse {
      val in = Done[JsValue, Unit]((), Input.EOF)
      val out = Enumerator[JsValue](JsObject(Seq("what" -> JsString("bye")))).andThen(Enumerator.eof)
      (in, out)
    }
  }

  def index = Action { implicit request =>
    Ok("all games")
  }

  private def send(channel: Channel[JsValue], data: (String, JsValue)*) =
    channel.push(JsObject(data))

  private def player(gameId: String, playerId: String, channel: Channel[JsValue]): Iteratee[JsValue, Unit] = Iteratee.foreach[JsValue] { message =>
    val key = (message \ "what").asOpt[String]
    Logger.debug(s"received order from $playerId: ${key.getOrElse("???")}")
    key match {
      case Some("say") =>
        val text = (message \ "text").asOpt[String].getOrElse("I'm gay!")
        channel.push(JsObject(Seq(
          "what" -> JsString("say"),
          "who" -> JsString(playerId),
          "text" -> JsString(text)
        )))

      case Some("update") =>
        games.get(gameId).fold() {g =>
          channel.push(g.gamersJson)
          channel.push(g.stateJson)
        }

      case Some("move") =>
        for {
          fr <- (message \ "fromRow").asOpt[Int]
          fc <- (message \ "fromCol").asOpt[Int]
          tr <- (message \ "toRow").asOpt[Int]
          tc <- (message \ "toCol").asOpt[Int]
          g <- games.get(gameId)
        } yield {
          if (g.state.currentWhite && playerId == g.white || (!g.state.currentWhite) && playerId == g.black) {
            Logger.debug(s"making a move (row=$fr, col=$fc) -> (row=$tr, col=$tc)")
            (remoteGames ? MakeAMove(gameId, Position(fr, fc), Position(tr, tc))).mapTo[MoveResult].map {
              case ValidMove(state) =>
                Logger.debug("..move was ok")
                val game = g.change(state)
                games(gameId) = game
                channel.push(game.stateJson)
              case InvalidMove(reason) =>
                Logger.debug(s"..move was wrong: $reason")
                channel.push(JsObject(Seq(
                  "what" -> JsString("info"),
                  "to" -> JsString(playerId),
                  "message" -> JsString(reason)
                )))
              case KingWasKilled =>
                Logger.debug("match has ended")
                channel.push(JsObject(Seq(
                  "what" -> JsString("finished")
                )))
            }
          } else {
            send(
              channel,
              "what" -> JsString("info"),
              "to" -> JsString(playerId),
              "message" -> JsString("it's not your turn")
            )
          }
        }
      case _ =>
        Logger.debug("There is something fishy around here. I'm in chess class.")
    }
  }.map { _ =>
    Logger.debug("someone have quitted")
  }

//  private def spectator(gameId: String, playerId: String, channel: Channel[JsValue]): Iteratee[JsValue, Unit] = Iteratee.foreach[JsValue] {
//    message =>
//      message \ "what" match {
//        case JsString("say") =>
//          val text = (message \ "text").asOpt[String].getOrElse("I'm gay!")
//          channel.push(JsObject(Seq(
//            "what" -> JsString("say"),
//            "who" -> JsString(playerId),
//            "text" -> JsString(text)
//          )))
//      }
//  }.map {
//    _ =>
//      Logger.debug("spectator quited")
//  }

}
