name := "client"

version := "1.0-SNAPSHOT"

resolvers += Resolver.file("Local Ivy2 Repository", file(System.getProperty("user.home") + "/.ivy2/local"))(Resolver.ivyStylePatterns)

libraryDependencies ++= Seq(
//  jdbc,
//  anorm,
//  cache,
  "com.typesafe.akka" %% "akka-remote" % "2.2.4",
  "com.mdraco" %% "chess-common" % "1.1-SNAPSHOT"
)     

play.Project.playScalaSettings
