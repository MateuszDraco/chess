function initBoard() {
    var board = $('#game #board table')
    var mapping = {
        1: "A",
        2: "B",
        3: "C",
        4: "D",
        5: "E",
        6: "F",
        7: "G",
        8: "H"
    }
    for (r = 9; r >= 0; r--) {
        var row = $('<tr></tr>')
        for (c = 0; c < 10; c++) {
            if (r == 0 || r == 9) {
                if (c > 0 && c < 9) {
                    row.append('<td class="o">' + mapping[c] + '</td>')
                } else {
                    row.append('<td class="o"></td>')
                }
            } else {
                if (c == 0 || c == 9) {
                    row.append('<td class="o">' + r + '</td>' )
                } else {
                    var k
                    if ((r + c) % 2 == 0) k = 'b'; else k = 'w';
                    row.append('<td class="' + k + '"></td>')
                }
            }
        }
        board.append(row)
    }
    assignCellsClick()
}

function initChat() {
	var WS = window['MozWebSocket'] ? MozWebSocket : WebSocket;
	var address = 'ws://' + window.location.host + '/game/' + gameId + '/open';

	chatSocket = new WS(address);

	chatSocket.onmessage = onMessage;
	chatSocket.onclose = onClose;
	chatSocket.onerror = onClose;
	chatSocket.onopen = onOpen;
}

function onOpen() {
    $("#game #chat input").prop('disabled', false);
    send({
        what: "update"
    })
}

function onClose() {
    $("#game #chat input").prop('disabled', true);
}

var gamers = {}

var sign = '&rsaquo; '

function onMessage(event) {
    var data = JSON.parse(event.data)

    switch (data.what) {
        case 'bye':
            window.location = 'http://' + window.location.host
            break
        case 'players':
            gamers = {}
            for (g in data.players) {
                addGamer(data.players[g])
            }
            break
        case 'state':
            $('.piece').remove()
            addPieces('w', data.state.whites)
            addPieces('b', data.state.blacks)
            break
        case 'say':
            addLine('<span>' + gamers[data.who] + '</span> &rarr; ' + data.text)
            break
        case 'info':
            console.log('info came')
            if (data.to == null || data.to == playerId) {
                addLine('<i>' + data.message + '</i>')
            }

    }
}

function addPieces(style, pieces) {
    for (i in pieces) {
        var p = pieces[i]
        addPiece(style, p.kind, p.row, p.col)
    }
}

function addPiece(style, name, row, col) {
    var tableRow = $('#game #board table tr')[8 - row]
    var rowCol = $('td', $(tableRow))[1 + col]

    $(rowCol).append('<span class="' + style + ' piece">' + name + '</span>')
}

function addLine(line, style) {
    var b = '<li>'
    if (style != null) b='<li class="' + style + '">'
    $("#chat ul").append(b + line + '</li>\n')
}

function addGamer(d) {
    gamers[d.id] = d.name
}

function send(obj) {
    chatSocket.send(JSON.stringify(obj));
}

function say(something) {
    if (something != null)
        send({
            what: 'say',
            text: String(something)
        })
}

function doSay() {
    var input = $('#chat input[type="text"]')[0]
    say(input.value)
    input.value = ''
}


function assignChat() {
    $('#chat input[type="button"]').click(function(event) {
        doSay()
        event.preventDefault()
    });

    $('#chat input[type="text"]').keypress(function(event) {
        if (event.charCode == 13 || event.keyCode == 13) {
            doSay()
            event.preventDefault()
        }
    })
}

function fieldClicked(field) {
    if (field.hasClass('active')) {
        field.removeClass('active')
    } else {
        var piece = getActivePiece()
        $('#game #board table tr td').removeClass('active')
        if (piece != null) {
            var tdFrom = $(piece).parent('td')
            var from = getPosition(tdFrom)
            var to = getPosition(field)
            console.log('move (' + from.col + ', ' + from.row + ') -> (' + to.col + ', ' + to.row + ')')
            send({
                what: "move",
                fromRow: from.row,
                fromCol: from.col,
                toRow: to.row,
                toCol: to.col
            })
        } else {
            console.log('select piece to move')
            field.addClass('active')
        }
    }
}

function getPosition(cell) {
    var tr = cell.parent('tr')
    var table = tr.parent()
    return {
        col: tr.children().index(cell) - 1,
        row: 8 - table.children().index(tr)
    }
}

function getActivePiece() {
    return $('#game #board table tr td.active span')[0]
}

function assignCellsClick() {
    $('#game #board table tr > td').click(function(event) {
        var cell = $(this)
        if (!cell.hasClass('o'))
            fieldClicked(cell)
        event.preventDefault()
    })
}

$(function() {
    initBoard()
    initChat()
    assignChat()
})