function initChat() {
    console.log('lets go!')
	var WS = window['MozWebSocket'] ? MozWebSocket : WebSocket
	chatSocket = new WS('ws://' + window.location.host + window.location.pathname + '/open');

	chatSocket.onmessage = onMessage;
	chatSocket.onclose = onClose;
	chatSocket.onerror = onClose;
	chatSocket.onopen = onOpen;
}

function onOpen() {
    console.log('chat is just fine')
    $("#anteroom #chat input").prop('disabled', false);
}

function onClose() {
    $("#anteroom #chat input").prop('disabled', true);
    //window.location = "http://" + window.location.host
}

var myNameIs = "SlimShady"
var myIdIs = ""
var gamers = {}

var sign = '&rsaquo; '

function onMessage(event) {
    var data = JSON.parse(event.data)

    switch (data.what) {
        case 'welcome':
            myNameIs = data.me.name
            myIdIs = data.me.id
            addLine(sign + 'Hello, hello <b>' + myNameIs + '</b>. Feel free to talk or pick a match with someone.', 'i')
            var header = $('#anteroom h1')
            $('span', header).text(myNameIs)
            header.slideDown()
            break
        case 'gamers':
            console.log('here we come!')
            gamers = {}
            gamers[myIdIs] = myNameIs
            for (g in data.gamers) {
                addGamer(data.gamers[g])
            }
            assignGamersClick()
            break
        case 'greenhorn':
            addGamer(data.who)
            addLine(sign + data.who.name + ' have entered the arena!', 'i')
            assignGamersClick()
            break
        case 'quited':
            var name = gamers[data.who]
            delete gamers[data.who]
            if (name != null)
                addLine(sign + name + ' have left. What a looser.', 'i')
            //console.log('to remove: #anteroom #gamers #' + data.who)
            $('#anteroom #gamers #' + data.who).slideUp().remove()
            break
        case 'rename':
            gamers[data.who] = data.name
            break
        case 'start':
            startAMatch(data.match)
            break
        case 'matchStarted':
            addLine(sign + 'A match just started between <b>' + gamers[data.gamer1] + '</b> and <b>' + gamers[data.gamer2] + '</b>', 'i')
            break
        case 'say':
            addLine('<span>' + gamers[data.who] + '</span> &rarr; ' + data.said)
            break

    }
}

function startAMatch(matchId) {
    var address = 'http://' + window.location.host + '/game/' + matchId + '/' + myNameIs
    //console.log(address)
    //alert(address)
    window.location = address
}

function addLine(line, style) {
    var b = '<li>'
    if (style != null) b='<li class="' + style + '">'
    $("#anteroom #chat ul").append(b + line + '</li>\n')
}

function addGamer(d) {
    gamers[d.id] = d.name
    $('#anteroom #gamers ul').append('<li id="' + d.id + '">' + d.name + '</li>')
}

function send(obj) {
    chatSocket.send(JSON.stringify(obj));
}

function say(something) {
    console.log('esad: ' + something)
    if (something != null)
        send({
            what: 'say',
            text: String(something)
        })
}

function doSay() {
    var input = $('#anteroom #chat input[type="text"]')[0]
    say(input.value)
    input.value = ''
}

function assignGamersClick() {
    $('#anteroom #gamers ul li').click(function(event) {
        event.preventDefault()
        //console.log('clicked: ' + event.target.id)
        send({
            what: "start",
            opponent: String(event.target.id)
        })
    })
}

function assignChat() {
    $('#anteroom #chat input[type="button"]').click(function(event) {
        doSay()
        event.preventDefault()
    });

    $('#anteroom #chat input[type="text"]').keypress(function(event) {
        if (event.charCode == 13 || event.keyCode == 13) {
            doSay()
            event.preventDefault()
        }
    })
}

$(function() {
    console.log('initializing....')
    initChat()

    assignChat()

    assignGamersClick()
})
