package chess.common.events

/**
 *
 * User: mateusz
 * Date: 25.04.2014
 * Time: 23:53
 * Created with IntelliJ IDEA.
 */
case class CloseAGame(gameId: String) extends ChessRequest
