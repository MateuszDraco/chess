package chess.common.events

import chess.common.data.Position

/**
 *
 * User: mateusz
 * Date: 25.04.2014
 * Time: 23:29
 * Created with IntelliJ IDEA.
 */
case class MakeAMove(gameId: String, from: Position, to: Position) extends ChessRequest