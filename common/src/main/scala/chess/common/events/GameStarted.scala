package chess.common.events

import chess.common.data.GameState

/**
 *
 * User: mateusz
 * Date: 25.04.2014
 * Time: 23:29
 * Created with IntelliJ IDEA.
 */
case class GameStarted(id: String, state: GameState) extends ChessResponse
