package chess.common.events

import chess.common.data.GameState

/**
 *
 * User: mateusz
 * Date: 25.04.2014
 * Time: 23:49
 * Created with IntelliJ IDEA.
 */
trait MoveResult extends ChessResponse

case class ValidMove(state: GameState) extends MoveResult

case class InvalidMove(reason: String) extends MoveResult

case object KingWasKilled extends MoveResult
