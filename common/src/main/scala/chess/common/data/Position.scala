package chess.common.data

/**
 *
 * User: mateusz
 * Date: 25.04.2014
 * Time: 19:38
 * Created with IntelliJ IDEA.
 */
case class Position(row: Int, col: Int) {
  def up: Option[Position] =
    if (row > 0)
      Some(Position(row - 1, col))
    else
      None

  def right: Option[Position] =
    if (col < 7)
      Some(Position(row, col + 1))
    else
      None

  def down =
    if (row < 7)
      Some(Position(row + 1, col))
    else
      None

  def left =
    if (col > 0)
      Some(Position(row, col - 1))
    else
      None

  def move(directions: String): Option[Position] =
    directions.foldLeft[Option[Position]](Some(this))((result: Option[Position], direction: Char) => result match {
      case None => None
      case Some(position) => position.move(direction)
    })

  def move(direction: Char): Option[Position] = direction match {
    case 'u' => up
    case 'r' => right
    case 'd' => down
    case 'l' => left
  }
}