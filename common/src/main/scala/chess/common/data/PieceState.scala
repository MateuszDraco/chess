package chess.common.data

/**
 *
 * User: mateusz
 * Date: 25.04.2014
 * Time: 19:37
 * Created with IntelliJ IDEA.
 */
case class PieceState(kind: String, position: Position)