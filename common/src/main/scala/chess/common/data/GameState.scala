package chess.common.data

/**
 *
 * User: mateusz
 * Date: 25.04.2014
 * Time: 19:16
 * Created with IntelliJ IDEA.
 */
case class GameState(currentWhite: Boolean, whites: List[PieceState], blacks: List[PieceState]) {
  def white(position: Position): Boolean = whites.exists(_.position == position)
  def black(position: Position): Boolean = blacks.exists(_.position == position)
  def any(position: Position): Boolean = white(position) || black(position)
}

